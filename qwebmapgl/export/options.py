import os
from ..utils import urljoin


class ExportOptions:
    def __init__(self, map, settings, **kwargs):
        self.map = map
        self.settings = settings
        self.base_path = kwargs.get('base_path', None)
        self.base_url = kwargs.get('base_url', None)
        self.data_dir = kwargs.get('data_dir', 'data')
        self.legend_dir = kwargs.get('legend_dir', 'legend')
        self.legend_format = kwargs.get('legend_format', 'SVG')
        self.style_dir = kwargs.get('style_dir', 'style')
        self.tile_name = kwargs.get('tile_name', 'tiles')
        self.style_name = kwargs.get('style_name', 'style')
        self.open_browser = kwargs.get('open_browser', False)
        self.preview = kwargs.get('preview', False)
        self.sprite_scales = kwargs.get('sprite_scales', [1, 2])
        self.sprite_name = kwargs.get('sprite_name', 'sprite')

        if self.base_path is None:
            self.base_path = os.path.join(settings.base_path, map.id)

        if self.base_url is None:
            self.base_url = urljoin(settings.base_url, map.id)

    @property
    def style_path(self):
        return os.path.join(
            self.base_path, self.style_dir, f'{self.style_name}.json')

    @property
    def style_dir_path(self):
        return os.path.join(self.base_path, self.style_dir)

    @property
    def data_path(self):
        return os.path.join(self.base_path, self.data_dir)

    @property
    def legend_path(self):
        return os.path.join(self.base_path, self.legend_dir)

    @property
    def legend_url(self):
        return urljoin(self.base_url, self.legend_dir)

    @property
    def tile_path(self):
        return os.path.join(
            self.data_path, f'{self.tile_name}.mbtiles')

    @property
    def data_url(self):
        return urljoin(self.base_url, self.data_dir)

    @property
    def style_url(self):
        return urljoin(
            self.base_url,
            self.style_dir,
            f'{self.style_name}.json'
        )

    @property
    def sprite_url(self):
        return urljoin(
            self.base_url,
            self.style_dir,
            self.sprite_name
        )

    @property
    def tile_base_url_relative(self):
        return urljoin('', self.data_dir, self.tile_name)

    @property
    def tile_base_url(self):
        return urljoin(self.data_url, self.tile_name)

    @property
    def tilejson_url(self):
        return urljoin(self.data_url, f'{self.tile_name}.json')

    @property
    def tilejson_url_relative(self):
        return urljoin('', self.data_dir, f'{self.tile_name}.json')
