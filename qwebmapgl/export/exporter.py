import os
from time import time
from qgis.core import QgsApplication, QgsTask
try:
    from ..qgsgl import Style, GeoJSONSource, VectorSource
except ImportError:
    from qgsgl import Style, GeoJSONSource, VectorSource
from .create_folders_task import CreateFoldersTask
from .export_task import ExportTask
from .render_templates_task import RenderTemplatesTask
from .write_legend_task import WriteLegendTask
from .write_source_task import WriteSourceTask
from .write_sprite import WriteSpriteTask
from .write_style_task import WriteStyleTask
from ..utils import tr


class Exporter:
    def __init__(self, options):
        self._options = options
        self._task = None

    def _add_vector_layer(self, style, source, layer):
        min_zoom, max_zoom = layer.map_zoom
        min_tile, max_tile = layer.tile_zoom

        # TODO: Add layer description from some QGIS layer field?
        source.add_layer(
            layer.qgis_layer,
            id=layer.id,
            min_zoom=min_tile,
            max_zoom=max_tile,
            attributes=layer.map_attributes)

        return style.add_layer(
            layer.qgis_layer, source.id, layer.id,
            min_zoom=min_zoom,
            max_zoom=max_zoom,
            legend=layer.legend,
            visible=layer.visible)

    def _export_task(self):
        return ExportTask(tr('Export web map'), self._options)

    def _folders_task(self):
        return CreateFoldersTask(
            tr('Create web map folder structure'), self._options)

    def _geojson_task(self, layer):
        geojson_path = os.path.join(
            self._options.data_path,
            '{}.geojson'.format(layer.id))
        source = GeoJSONSource(layer.id)
        source.add_layer(layer.qgis_layer, id=layer.id,
                         attributes=layer.download_attributes)
        description = tr('Write GeoJSON for layer {}').format(layer.id)
        return WriteSourceTask(description, source, geojson_path)

    def _legend_task(self, style):
        return WriteLegendTask(
            tr('Write legend images'),
            self._options, style)

    def _sprite_task(self, style, scale):
        return WriteSpriteTask(
            tr('Write sprites at scale {}'.format(scale)),
            style, scale, self._options)

    def _style_task(self, style):
        return WriteStyleTask(
            tr('Write style JSON'), style, self._options.style_path)

    def _templates_task(self, style, layer_map, revision):
        return RenderTemplatesTask(
            tr('Render web map templates'),
            self._options, layer_map, style.legend, revision)

    def _vector_task(self, source):
        return WriteSourceTask(
            tr('Write tiles'), source, self._options.tile_path)

    def export(self):
        revision = int(time())
        pdost = QgsTask.ParentDependsOnSubTask
        self._task = self._export_task()

        folders = self._folders_task()
        self._task.addSubTask(folders, [], pdost)

        layer_map = {}
        style = Style(
            glyphs_url=self._options.settings.glyphs_url,
            sprite_url=f'{self._options.sprite_url}?rev={revision}')

        if self._options.map.layers:
            vector_source = VectorSource(
                'data', f'{self._options.tilejson_url}?rev={revision}')
            style.add_source(vector_source)

            for layer in self._options.map.layers:
                if layer.map:
                    layer_map[layer.id] = self._add_vector_layer(
                        style, vector_source, layer)

                if layer.download:
                    geojson_task = self._geojson_task(layer)
                    self._task.addSubTask(geojson_task, [folders], pdost)

            vector_task = self._vector_task(vector_source)
            self._task.addSubTask(vector_task, [folders], pdost)

        style_task = self._style_task(style)
        self._task.addSubTask(style_task, [folders], pdost)

        for scale in self._options.sprite_scales:
            sprite_task = self._sprite_task(style, scale)
            self._task.addSubTask(sprite_task, [folders], pdost)

        legend_task = self._legend_task(style)
        self._task.addSubTask(legend_task, [folders], pdost)

        templates = self._templates_task(style, layer_map, revision)
        self._task.addSubTask(templates, [folders], pdost)

        QgsApplication.taskManager().addTask(self._task)
