from qgis.core import QgsTask, QgsMessageLog, Qgis

CATEGORY = 'Plugins'


class BaseTask(QgsTask):
    """Task to create web map output folders"""

    def __init__(self, description):
        super().__init__(description, QgsTask.CanCancel)
        self._exception = None
        self.result = None

    def _info(self, msg):
        QgsMessageLog.logMessage(
            'Web Map: {}'.format(msg), CATEGORY, Qgis.Info)

    def _run(self):
        raise NotImplementedError

    def run(self):
        self._info('Started task "{}"'.format(self.description()))
        try:
            self._run()
            return True
        except Exception as e:
            self._exception = e
            return False

    def finished(self, result):
        self._info('Finished task "{}"'.format(self.description()))
        self.result = result
        if self._exception:
            raise self._exception
