from .base_task import BaseTask


class WriteSourceTask(BaseTask):
    """Task to export a source"""

    def __init__(self, description, source, path):
        super().__init__(description)
        self._source = source
        self._path = path

    def _run(self):
        self._source.write(self._path)
