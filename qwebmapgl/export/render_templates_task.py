import os
import shutil
from .base_task import BaseTask
from .template_renderer import TemplateRenderer


class RenderTemplatesTask(BaseTask):
    """Task to render jinja2 templates"""

    def __init__(self, description, options, layer_map, legend, revision):
        super().__init__(description)
        self._options = options
        self._renderer = TemplateRenderer(options, layer_map, legend, revision)

    def _run(self):
        template = self._options.map.template

        for filename in template.config['render']:
            if self.isCanceled():
                return False
            out_path = os.path.join(self._options.base_path, filename)
            self._renderer.stream(filename).dump(out_path)

        for filename in template.config['copy']:
            if self.isCanceled():
                return False
            in_path = os.path.join(template.folder, filename)
            out_path = os.path.join(self._options.base_path, filename)

            if os.path.isfile(in_path):
                shutil.copy(in_path, out_path)
            elif os.path.isdir(in_path):
                if os.path.isdir(out_path):
                    shutil.rmtree(out_path)
                shutil.copytree(in_path, out_path)

        return True
