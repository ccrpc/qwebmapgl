from .base_setting import BaseSetting


class StringSetting(BaseSetting):
    def __init__(self, name, default=None):
        self.name = name
        self.default = default

    def key(self):
        return 'qwebmapgl/{}'.format(self.name)

    def read(self, obj):
        value = obj.settings.value(self.key(), self.default)
        self.__set__(obj, value)

    def write(self, obj):
        value = self.__get__(obj)
        obj.settings.setValue(self.key(), value)
