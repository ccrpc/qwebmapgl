import json
import os


class Template:

    def __init__(self, id, folder):
        self.id = id
        self.folder = folder
        self.config = self._get_config()

    def _get_config(self):
        config = {
            'name': 'Template',
            'render': ['index.html'],
            'copy': []
        }
        config.update(self._read_config_json())
        return config

    def _read_config_json(self):
        config_path = os.path.join(self.folder, 'template.json')

        if not os.path.isfile(config_path):
            return {}

        with open(config_path) as config_file:
            return json.load(config_file)

    @property
    def name(self):
        return self.config.get('name', 'Template')
