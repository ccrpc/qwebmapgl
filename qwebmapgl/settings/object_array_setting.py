from PyQt5.QtCore import QSettings
from .base_setting import BaseSetting


class ObjectArraySetting(BaseSetting):
    def __init__(self, name, cls, attributes):
        super().__init__(name)
        self.cls = cls
        self.attributes = attributes
        self.default = []

    def group(self):
        return 'qwebmapgl/{}'.format(self.name)

    def read(self, obj):
        settings = QSettings()
        count = settings.beginReadArray(self.group())
        items = []
        for i in range(count):
            settings.setArrayIndex(i)
            attrs = [settings.value(a) for a in self.attributes]
            items.append(self.cls(*attrs))
        settings.endArray()
        self.__set__(obj, items)

    def write(self, obj):
        items = self.__get__(obj)
        settings = QSettings()
        settings.beginWriteArray(self.group())
        for i, item in enumerate(items):
            settings.setArrayIndex(i)
            for attr in self.attributes:
                settings.setValue(attr, getattr(item, attr))
        settings.endArray()
