from PyQt5.QtCore import QCoreApplication


def tr(message):
    return QCoreApplication.translate('WebMapGLExporter', message)


def urljoin(*parts):
    return '/'.join([p.strip('/') for p in parts])
