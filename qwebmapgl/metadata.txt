[general]
name=Web Map Exporter
qgisMinimumVersion=3.6
description=Exports a web map using Web Map GL
version=0.2.4
author=Champaign County Regional Planning Commission
email=info@ccrpc.org
about=Web Map GL Exporter allows users to export a QGIS map for the web using Web Map GL.
tracker=https://gitlab.com/ccrpc/qwebmapgl/issues
repository=https://gitlab.com/ccrpc/qwebmapgl
changelog=https://gitlab.com/ccrpc/qwebmapgl/blob/master/CHANGELOG.md
tags=python, web, map, GL
homepage=https://gitlab.com/ccrpc/qwebmapgl
category=Web
icon=resources/icon.svg
experimental=True
deprecated=False
