import math
from qgis.core import QgsVectorLayer, QgsCoordinateTransform, \
    QgsCoordinateReferenceSystem, QgsPoint
from .layer import WebMapLayer


class WebMap:
    _project_settings = [
        'base_map_url', 'center_latitude', 'center_longitude', 'id',
        'initial_zoom', 'template_id']

    def __init__(self, project, canvas, settings):
        self._project = project
        self._canvas = canvas
        self.layers = list(self._create_layers())
        self.title = project.title()
        self.plugin_settings = settings
        self._read_project_settings()

    def _read(self, name, default, converter=None):
        if converter is None:
            converter = type(default)
        return converter(
            self._project.readEntry('qwebmapgl', name, str(default))[0])

    def _read_project_settings(self):
        position = self.get_canvas_position()
        default_base_map = self.plugin_settings.default_base_map
        default_base_map_url = default_base_map.url if default_base_map else ''
        default_template = self.plugin_settings.default_template
        default_template_id = default_template.id \
            if default_template else ''

        self.base_map_url = self._read('base_map_url', default_base_map_url)
        self.center_latitude = self._read(
            'center_latitude', position['latitude'], float)
        self.center_longitude = self._read(
            'center_longitude', position['longitude'], float)
        self.initial_zoom = self._read('initial_zoom', position['zoom'], float)
        self.id = self._read('id', '')
        self.output_folder = self._read('output_folder', '')
        self.template_id = self._read('template_id', default_template_id)

    def _write_project_settings(self):
        for attr in self._project_settings:
            self._project.writeEntry(
                'qwebmapgl', attr, str(getattr(self, attr)))

    def _create_layers(self):
        for layer in self._project.layerTreeRoot().layerOrder():
            if isinstance(layer, QgsVectorLayer):
                yield WebMapLayer(self, layer)

    @property
    def template(self):
        return self.plugin_settings.get_template(self.template_id)

    def get_center_point(self):
        # https://github.com/qgis/QGIS/issues/32443
        # https://gitlab.com/ccrpc/qwebmapgl/issues/29
        # * Workaround to create QgsPoint from X, Y instead of QgsPointXY
        center = QgsPoint(self._canvas.center().x(), self._canvas.center().y())
        transform = QgsCoordinateTransform(
            self._project.crs(),
            QgsCoordinateReferenceSystem('EPSG:4326'),
            self._project)
        center.transform(transform)
        return center

    def get_canvas_position(self):
        center = self.get_center_point()

        return {
            'latitude': center.y(),
            'longitude': center.x(),
            'zoom': self.scale_to_zoom(self._canvas.scale(), center.y())
        }

    def use_canvas_position(self):
        position = self.get_canvas_position()
        self.center_latitude = position['latitude']
        self.center_longitude = position['longitude']
        self.initial_zoom = position['zoom']

    def scale_to_zoom(self, scale, lat):
        # Based on: https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
        settings = self._canvas.mapSettings()
        dpi = settings.outputDpi()
        return math.log(
            dpi * 39.37 * 156543.03 * abs(math.cos(lat)) / scale, 2)

    def save(self):
        self._write_project_settings()
        self._project.setTitle(self.title)
        for layer in self.layers:
            layer.save()
