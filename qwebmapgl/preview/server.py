import socketserver
from http.server import HTTPServer


class PreviewHTTPServer(socketserver.ThreadingMixIn, HTTPServer):
    daemon_threads = True

    def __init__(self, server_address, RequestHandlerClass, options):
        self._host = server_address[0]
        self.tile_name = options.tile_name
        self.tile_path = options.tile_path
        self.tile_base_url_relative = options.tile_base_url_relative
        self.tilejson_url_relative = options.tilejson_url_relative

        map = options.map
        self.position = [
            map.center_longitude, map.center_latitude, map.initial_zoom]

        super().__init__(server_address, RequestHandlerClass)

    @property
    def base_url(self):
        return 'http://{}:{}'.format(self._host, self.server_port)
