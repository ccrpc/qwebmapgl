def classFactory(iface):
    from .plugin import WebMapGLExporter
    return WebMapGLExporter(iface)
