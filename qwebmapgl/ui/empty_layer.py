
class EmptyLayer:

    def __init__(self):
        self.id = ''
        self.name = ''
        self.fields = []
        self.map = False
        self.legend = False
        self.download = False
        self.popup = False
        self.min_zoom = 0
        self.max_zoom = 0
