class TableColumn:
    def __init__(self, attr, label, **kwargs):
        self.attr = attr
        self.label = label
        self.enabled = kwargs.get('enabled', True)
        self.editable = kwargs.get('editable', True)
        self.checkbox = kwargs.get('checkbox', False)
