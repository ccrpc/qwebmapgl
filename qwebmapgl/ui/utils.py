import os
from PyQt5 import uic
from ..utils import tr


def load_form_ui(filename):
    return uic.loadUiType(os.path.join(os.path.dirname(__file__), filename))


def add_menu_action(menu, label, handler):
    return menu.addAction(tr(label)).triggered.connect(handler)
