# Web Map Exporter

QGIS 3 plugin to create web maps using [Web Map GL][1].

## Credits
Web Map Exporter was developed by Matt Yoder for the [Champaign County
Regional Planning Commission][2].

## License
Web Map Exporter is available under the terms of the [BSD 3-clause
license][3].

[1]: https://gitlab.com/ccrpc/webmapgl
[2]: https://ccrpc.org/
[3]: https://gitlab.com/ccrpc/qwebmapgl/blob/master/LICENSE.md
